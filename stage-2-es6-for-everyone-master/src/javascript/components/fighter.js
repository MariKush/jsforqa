export class Fighter {
    constructor( name, health, attack, defense) {
        this.name = name;
        this.health = health;
        this.attack = attack;
        this.defense = defense;
    }

    getHitPower(){
        let criticalHitChance = Math.random()+1;
        return attack * criticalHitChance;
    }

    getBlockPower(){
        let dodgeChance = Math.random()+1;
        return defense * dodgeChance;
    }
  
  }