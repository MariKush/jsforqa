import { callApi } from '../helpers/apiHelper';
import { Fighter } from '../components/fighter';

class FighterService {
  async getFighters() {
    try {
      const endpoint = 'fighters.json';
      const apiResult = await callApi(endpoint, 'GET');

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id) {
    var response = JSON.parse(`details/fighter/${id}.json`);
    var name = response.name;
    var health = response.health;
    var attack = response.attack;
    var defence = response.defence;
    var source = response.source;
  
    return new Fighter(name, health, attack, defence, source);
  }
}

export const fighterService = new FighterService();
