//task1
function f(){
    let arr = document.getElementsByClassName("bubble");
    for (let i=arr.length-1;i>=0;i--)
        arr[i].click();
    setTimeout(function () { f(); }, 0);
}

f();

//task2
function sort(str){
  str=str.replace(/\s+/g,' ')
  let arr=str.split(' ');
  var items=[];
  for (let i=0;i<arr.length;i++){
    items.push({value: arr[i], key: arr[i].replace(/[0-9]/g, '')});
  }
  items.sort(function(a, b) {
  if (a.key < b.key) {
    return -1;
  }
  if (a.key > b.key) {
    return 1;
  }
  return 0;
  });
  for (let i=0;i<arr.length;i++){
    arr[i]=items[i].value;
  }
  return arr.join(' ');
}

//task3
//if a>b return value >0;
//if a<b return value <0;
//if a==b return 0
function separator(a){
  let separators=['.',',','/','-'];
  for(let i=0;i<separators.length;i++){
    if (a.includes(separators[i]))
      return separators[i];
  }
}

function getYear(a, separator){
  let arr = a.split(separator);
  for (let i = 0; i < arr.length; i++) {
    if (arr[i].length>=4) 
      return arr[i].trim();
  }
}

function getMonth(a, separator){
  let arr = a.split(separator);
  for (let i = 0; i < arr.length; i++) {
    if (arr[i].length==2) 
      return arr[i].trim();
  }
}

function getDay(a, separator){
  let arr = a.split(separator);
  for (let i = 0; i < arr.length; i++) {
    if (arr[i].length==3) 
      return arr[i].trim();
  }
}

function compare(a, b){
  let aSeparator=separator(a);
  let bSeparator=separator(b);
  let result = getYear(a,aSeparator)-getYear(b,bSeparator);
  if (result==0){
    result = getMonth(a,aSeparator)-getMonth(b,bSeparator);
    if (result == 0) 
      result = getDay(a,aSeparator)-getDay(b,bSeparator);
  }
  return result;
}

//task4
function searchUnic(arr){
  let def;
  if (arr[0] == arr[1] || arr[0] == arr[2]){
    def=arr[0];
  } else if (arr[1] == arr[2]){
    def=arr[1];
  }
  
  for (let i = 0; i < arr.length; i++) {
    if (arr[i]!=def)
          return arr[i];
  }
}


//task5
//ми не знаєскільки коштує товар, тому ми не можемо визначити його вартість, 
//але можна занйти знижку на товар, в залежності від його кількості (про це якраз говорить назва функції)
function getDiscount(n){
  if (n<5) return 0;
  if (n<10) return 5;
  return 10;
}


//task6
function getDaysForMonth(month){
  switch (month) {
    case 2:
      return 28;
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
      return 31;
    default:
      return 30;
    }
}